// Теоретичне питання:
// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна?

// Деструктуризація - це функція в мовах програмування, яка дозволяє витягувати значення з масивів,
// об'єктів або інших структур даних в окрумі змінні. Деструктуризація потрібна, оскільки вона спрощує
// читабильність коду; можна звертатись не до окремого значення, а до деструктувати структуру даних
// на окрумі змінні; можна вибірково витягувати лише потрібні значення, ігноруючи іншу.

// Практичні завдання:
// Завдання 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const clients3 = [...clients1, "Зальцман", "Майклсон"];

console.log(clients3);

// Завдання 2:
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  const charactersShortInfo = characters.map(({name, lastName, age}) => ({name, lastName, age}));

  console.log(charactersShortInfo);

//   Завдання 3:
const user1 = {
    name: "John",
    years: 30
  };

  const {name, years: age, isAdmin = false} = user1;

  console.log(name);
  console.log(age);
  console.log(isAdmin);

//   Завдання 4:
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  };
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  };
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  };

  const fullProfile = Object.assign({}, satoshi2018, satoshi2019, satoshi2020);

  console.log(fullProfile);

//   Завдання 5:
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  };

  const updatedBooks = [...books, bookToAdd];
  
  console.log(updatedBooks);

//   Завдання 6:
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  };

const updatedEmployee = {
    ...employee,
    age: 50,
    salary: 100000,
};

console.log(updatedEmployee);

// Завдання 7:
const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); 
alert(showValue());  